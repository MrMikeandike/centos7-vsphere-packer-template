#!/bin/bash

echo "wgetting packer-vsphere" && wget https://github.com/jetbrains-infra/packer-builder-vsphere/releases/download/v2.3/packer-builder-vsphere-iso.linux \
&& echo "chmod to make it executable" && chmod +x packer-builder-vsphere-iso.linux \
&& echo "moving to /usr/local/bin" && mv packer-builder-vsphere-iso.linux /usr/local/bin/
