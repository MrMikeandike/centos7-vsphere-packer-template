export PACKER_RELEASE="1.4.5" \
&& echo "wgetting packer" && wget https://releases.hashicorp.com/packer/${PACKER_RELEASE}/packer_${PACKER_RELEASE}_linux_amd64.zip \
&& echo "unzip packer" && unzip packer_${PACKER_RELEASE}_linux_amd64.zip \
&& echo "moving packer to /usr/local/bin" && mv packer /usr/local/bin/
