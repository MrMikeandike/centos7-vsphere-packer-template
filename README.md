# Requirements

* packer
  * use tools/packer.sh to install (only tested on debian
  * I used 1.4.5
* packer vsphere-iso plugin from jetbrains
  * use tools/packer-vsphere.sh to install (only tested on debian)
* vsphere
  * I'm on 6.5
* centos iso on a vsphere datastore
  * I used centos7 1908 minimal iso. If you use another iso, make sure you update the checksum as well in variables

---

# Quickstart

1. Check requirements
2. run "cp variables-example.json variables.json" and fill it out
3. run "packer build variables.json"
    * you can also run "packer build --on-error=ask variables.json" or "PACKER_LOG=1 packer build --on-error=ask variables.json" for better debugging
4. Be happy. You can now build vm from template and ssh using username=dawson password=dawson rootpassword=dawson

---

# Helpful documentation

## Packer help
Check https://github.com/jetbrains-infra/packer-builder-vsphere for info on how the variables are used

Specifically, https://github.com/jetbrains-infra/packer-builder-vsphere#working-with-clusters should be looked at if you are using clusters or resource pools rather than host and default resource pool like I am

## Kickstart help

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/installation_guide/sect-kickstart-syntax (This requires a redhat login but its probably worth creating an account for)

---

# Other gotchas
 
## changing ssh username/password or root password

To change login info, you need to edit not only centos-cloudinit.json, but also the ks.cfg file.
